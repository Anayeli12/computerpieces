const anio = new Date().getFullYear();

const a = () => {
    const footer = document.createElement('footer');
    footer.style = 'margin-bottom: 10px; margin-top: 10px;';
    footer.innerHTML = ` &copy; ${anio} copyrigth ComputerPieces`;
    document.body.append(footer);
}

a();

// let stylesF = 'color: rgb(25, 100, 185); background: #1b9bff;';
let stylesF = 'color: white ; background: #1b9bff; transition: .5s;';

// URL http://funplayv1.herokuapp.com/
const urlHome = 'http://localhost:4000/';
const urlHomeP = 'https://computerpieces.herokuapp.com/';
const urlHomePS = 'https://computerpieces.herokuapp.com/';

const urlAbout = 'http://localhost:4000/about';
const urlAboutP = 'https://computerpieces.herokuapp.com/about';
const urlAboutPS = 'http://computerpieces.herokuapp.com/about';

const urlContact = 'http://localhost:4000/contact';
const urlContactP = 'https://computerpieces.herokuapp.com/contact';
const urlContactPS = 'http://computerpieces.herokuapp.com/contact';

const urlProduct = 'http://localhost:4000/product';
const urlProductP = 'https://computerpieces.herokuapp.com/product';
const urlProductPS = 'http://computerpieces.herokuapp.com/product';
// 'http://funplayv1.herokuapp.com/plays'

const urlNotes = 'http://localhost:4000/notes';
const urlNotesP = 'https://computerpieces.herokuapp.com/notes';
const urlNotesPS = 'http://computerpieces.herokuapp.com/notes';

// Id de la navegacion
const home = document.getElementById('home');
const about = document.getElementById('about');
const contact = document.getElementById('contact');
const plays = document.getElementById('plays');
const notes = document.getElementById('notes');
const product = document.getElementById('product');

// d.style.color = 'red';
// d.style = 'color: rgb(25, 100, 185); background: #1b9bff;';
// d.className = 'active';
const url = document.URL;
console.log(url);
if (url) {
    // HOme
    if (url == urlHome || url == urlHomeP || url == urlHomePS) {

        home.style = stylesF;
    } else {
        home.style = '';
    }
    // about
    if (url == urlAbout || url == urlAboutP || url == urlAboutPS) {

        about.style = stylesF;
    } else {
        about.style = '';
    }
    // contact
    if (url == urlContact || url == urlContactP || url == urlContactPS) {

        contact.style = stylesF;
    } else {
        // contact.style = '';
    }

    // notes
    if (url == urlNotes || url == urlNotesP || url == urlNotesPS) {
        notes.style = stylesF;
    } else {
        // notes.style = '';
    }
    // notes
    if (url == urlProduct || url == urlProductP || url == urlProductPS) {
        product.style = stylesF;
    } else {
        // notes.style = '';
    }
}
const express = require('express');
const router = express.Router();
const Note = require('../models/Note');
const Img = require('../models/Img');


const { isAuthenticated } = require('../helpers/auth');

router.get('/', (req, res) => {
    res.render('index');
});
// router.get('/start', (req, res) => {
//     res.render('start');
// });
// router.get('/plays', isAuthenticated, (req, res) => {
//     res.render('plays');
// });
router.get('/about', (req, res) => {
    res.render('about');
});
router.get('/contact', (req, res) => {
    res.render('contact');
});
router.get('/product', async(req, res) => {
    // const note = await Note.find();
    const notes = await Note.find().sort({ date: 'desc' });
    // const img = await Img.find();

    // res.render('product'), { notes };
    res.render('product', { notes });
});


module.exports = router;